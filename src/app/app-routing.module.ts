import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'authentication', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) }, 
  { path: 'studentmanagement', loadChildren: () => import('./studentmanagement/studentmanagement.module').then(m => m.StudentmanagementModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
