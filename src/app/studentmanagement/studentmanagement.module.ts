import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentmanagementRoutingModule } from './studentmanagement-routing.module';
import { StudentmanagementComponent } from './studentmanagement.component';


@NgModule({
  declarations: [StudentmanagementComponent],
  imports: [
    CommonModule,
    StudentmanagementRoutingModule
  ]
})
export class StudentmanagementModule { }
