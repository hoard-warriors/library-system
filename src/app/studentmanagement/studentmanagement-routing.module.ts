import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentmanagementComponent } from './studentmanagement.component';

const routes: Routes = [{ path: '', component: StudentmanagementComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentmanagementRoutingModule { }
